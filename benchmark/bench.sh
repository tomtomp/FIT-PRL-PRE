#!/bin/bash

if [ $# -lt 3 ]
then 
    echo "Usage: ./test.sh MIN_LENGTH MAX_LENGTH STEP AVG_RUNS";
    exit;
fi;

min_length=$1
max_length=$2
step=$3
avg_runs=$4

if [ ${min_length} -lt 2 ]; then
    echo "Minimal length is 2!"
    exit;
fi;

if [ ${min_length} -ge ${max_length} ]; then
    echo "Minimal length should be at most the same as the maximal length!"
    exit;
fi;

max_processors=$(( 2 * ${max_length} - 2 ))
echo "localhost slots=${max_processors}" > ./hostfile

mpic++ --prefix /usr/local/share/OpenMPI -DBENCH -o pro pro.cpp -O3
#mpic++ --prefix /usr/local/share/OpenMPI -DDEBUG -o pro pro.cpp -O3

for length in `seq ${min_length} ${step} ${max_length}`
do
    num_procs=$(( 2 * ${length} - 2));
    # Create random testing vector.
    input=`tr -dc A-Za-z0-9 </dev/urandom | head -c ${length}`
    result=0
    for run in `seq 1 1 ${avg_runs}`
    do 
        result_run=`mpirun --prefix /usr/local/share/OpenMPI --hostfile ./hostfile -np ${num_procs} pro ${input}`
        result=`echo ${result} + ${result_run} | bc -l`
    done
    result=`echo "${result} / ${avg_runs}" | bc -l`
    echo "${length};${result}"
    echo "${length};${result}" >> ./result.csv
done

# Cleanup
rm -f pro test_input hostfile

