/**
 * @file mss.cpp
 * @Author Tomas Polasek
 * @brief Implementation of Merge-splitting sort algorithm, using OpenMPI.
 */

#include <mpi.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <algorithm>
#include <cmath>
#include <iomanip>

/// Tag used for all messages.
static constexpr int MY_TAG{0};

/// Tag used for sending predecesor information.
static constexpr int TAG_PRED{10};

#ifdef DEBUG
/// Tag used for printing synchronization.
static constexpr int TAG_DEBUG_PRINT{999};
#endif

/// Tag used when exchanging messages during suffix sum algorithm.
static constexpr int TAG_BASE_SUFFIX_SUM{1000};
/// Tag used for sending successors during the suffix sum algorithm.
static constexpr int TAG_SUCC_SUFFIX_SUM{TAG_BASE_SUFFIX_SUM + 1};
/// Tag used for sending weights during the suffix sum algorithm.
static constexpr int TAG_WEIGHT_SUFFIX_SUM{TAG_BASE_SUFFIX_SUM + 2};
/// Tag used for sending predecessors during the suffix sum algorithm.
static constexpr int TAG_PRED_SUFFIX_SUM{TAG_BASE_SUFFIX_SUM + 3};

/// Number of parameters required by the application.
static constexpr int PARAM_NUM_REQUIRED{1};
/// Index of the parameter containing input string.
static constexpr std::size_t PARAM_INPUT{1};

/// Value used when error occurs.
static constexpr int ERROR_SIZE{-1};

#ifdef DEBUG
/// Start of ordered debug printing block.
void debugPrintStart(std::size_t myId)
{
    if (myId == 0)
    {
        return;
    }

    int buf{0};
    MPI_Recv(&buf, 1, MPI_INT, 0, TAG_DEBUG_PRINT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/// End of ordered debug printing block.
void debugPrintEnd(std::size_t myId, std::size_t numProcs)
{
    int buf{0};
    if (myId == 0)
    {
        for (std::size_t proc = 1; proc < numProcs; ++proc)
        {
            MPI_Send(&buf, 1, MPI_INT, proc, TAG_DEBUG_PRINT, MPI_COMM_WORLD);
            MPI_Recv(&buf, 1, MPI_INT, proc, TAG_DEBUG_PRINT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
    }
    else
    {
        MPI_Send(&buf, 1, MPI_INT, 0, TAG_DEBUG_PRINT, MPI_COMM_WORLD);
    }
}

/// Get pre-order for given input, computed using sequential algorithm.
std::string getComparisonPreorder(const std::string &input)
{
    std::vector<char> result;
    std::vector<bool> directions;
    bool pastDown{true};
    std::size_t currentNode{1};

    while (result.size() != input.size())
    {
        if (pastDown)
        {
            result.push_back(input[currentNode - 1]);
            if (2 * currentNode <= input.size())
            {
                directions.push_back(true);
                currentNode = 2 * currentNode;
                pastDown = true;
            }
            else
            {
                currentNode = currentNode / 2;
                pastDown = false;
            }
        }
        else
        {
            if (directions.back() && (2 * currentNode + 1) <= input.size())
            {
                directions.pop_back();
                directions.push_back(false);
                currentNode = 2 * currentNode + 1;
                pastDown = true;
            }
            else
            {
                directions.pop_back();
                currentNode = currentNode / 2;
                pastDown = false;
            }
        }
    }

    return std::string(result.begin(), result.end());
}
#endif

/// Get parent node index for given edge.
std::size_t edgeParent(std::size_t edge)
{
    return (edge - 1) / 4 + 1;
}

/// Get child node index for given edge.
std::size_t edgeChild(std::size_t edge)
{
    return (edge - 1) / 2 + 2;
}

/// Is given edge a forward edge?
bool isEdgeForward(std::size_t edge)
{
    // Odd numbered edges are forward edges.
    return edge % 2 == 1;
}

/// Is given node the root node?
bool isRoot(std::size_t u)
{
    return u == 1;
}

/// Get source node index for given edge.
std::size_t edgeSrc(std::size_t edge)
{
    return isEdgeForward(edge) ? edgeParent(edge) : edgeChild(edge);
}

/// Get destination node index for given edge.
std::size_t edgeDst(std::size_t edge)
{
    return isEdgeForward(edge) ? edgeChild(edge) : edgeParent(edge);
}

/// Get reverse edge ID for given edge.
std::size_t reverseEdge(std::size_t edge)
{
    return isEdgeForward(edge) ? edge + 1 : edge - 1;
}

/**
 * Get edge, which goes from given node to its parent.
 * @warning Returns 0 if u is less than 2!
 */
std::size_t edgeToParent(std::size_t u)
{
    return !isRoot(u) ? 2 * (u - 2) + 2 : 0;
}

/// Get left child of given node.
std::size_t leftChild(std::size_t u)
{
    return 2 * u;
}

/// Get right child of given node.
std::size_t rightChild(std::size_t u)
{
    return 2 * u + 1;
}

/// Get edge to left child.
std::size_t edgeToLeftChild(std::size_t u)
{
    return 4 * (u - 1) + 1;
}

/// Get edge to right child.
std::size_t edgeToRightChild(std::size_t u)
{
    return 4 * (u - 1) + 3;
}

/// Get the value of first function for the Euler path.
std::size_t eFirst(std::size_t u)
{
    // First is edge to parent, or left child for root.
    return !isRoot(u) ? edgeToParent(u) : edgeToLeftChild(u);
}

/// Is u node father of the v node?
bool isFather(std::size_t u, std::size_t v)
{
    return u == v / 2;
}

/// Is u node left child of v node?
bool isLeftChild(std::size_t u, std::size_t v)
{
    return u == leftChild(v);
}

/// Is u node right child of v node?
bool isRightChild(std::size_t u, std::size_t v)
{
    return u == rightChild(v);
}

/**
 * Get the value of next function for the Euler path.
 * @return Returns the next edge, or 0 if there is no next.
 */
std::size_t eNext(std::size_t u, std::size_t v)
{
    if (isFather(v, u))
    {
        return edgeToLeftChild(u);
    }
    else if (isLeftChild(v, u))
    {
        return edgeToRightChild(u);
    }
    else
    {
        return 0;
    }
}

// Overflow-checked Euler next function, returns 0 if overflowed.
std::size_t ceilENext(std::size_t u, std::size_t v, std::size_t numEdges)
{
    std::size_t next{eNext(u, v)};
    return next > numEdges ? 0 : next;
}

/// Get next edge ID in Euler path.
std::size_t eTour(std::size_t edge, std::size_t numEdges)
{
    std::size_t u{edgeSrc(edge)};
    std::size_t v{edgeDst(edge)};
    std::size_t first{eFirst(v)};

    std::size_t next{ceilENext(v, u, numEdges)};

#ifdef DEBUG
    debugPrintStart(edge - 1);
    std::cout << "Edge: " << edge
              << " u: " << u
              << " v: " << v
              << " first(" << u << ") = " << first
              << " next(" << v << ", " << u << ") = " << next << std::endl;
    debugPrintEnd(edge - 1, numEdges);
#endif

    return next ? next : first;
}

/// Get edge ID for the edge going back to the root.
std::size_t edgeBackToRoot(std::size_t numEdges)
{
    return numEdges > 2 ? 4 : 2;
}

/// Calculate suffix sum with weight, using the Euler tour pointer and return the result.
std::size_t suffixSums(std::size_t myEdge, std::size_t myEtour, std::size_t myWeight, std::size_t numEdges)
{
    std::size_t numSteps{static_cast<std::size_t>(std::ceil(log2f(numEdges)))};

    std::size_t mySucc{myEtour};
    std::size_t myPred{0};

#ifdef DEBUG
    debugPrintStart(myEdge - 1);
    if (myEtour != myEdge)
    {
        std::cout << "I am " << myEdge << " sending " << myEdge << " to " << myEtour << std::endl;
    }
    if (myEdge != 1)
    {
        std::cout << "I am " << myEdge << " receiving predecesor..." << std::endl;
    }
    debugPrintEnd(myEdge - 1, numEdges);
#endif

    MPI_Request req;

    if (myEtour != myEdge)
    {
        MPI_Isend(&myEdge, 1, MPI_UINT64_T, myEtour - 1, TAG_PRED, MPI_COMM_WORLD, &req);
    }
    if (myEdge != 1)
    {
        MPI_Recv(&myPred, 1, MPI_UINT64_T, MPI_ANY_SOURCE, TAG_PRED, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    if (myEdge == 1)
    { // The first one, end the list.
        myPred = 0;
    }
    if (myEdge == edgeBackToRoot(numEdges))
    { // The last on, end the list.
        mySucc = 0;
    }

#ifdef DEBUG
    debugPrintStart(myEdge - 1);
    std::cout << "I am " << myEdge << " succ: " << mySucc << " pred: " << myPred << std::endl;
    debugPrintEnd(myEdge - 1, numEdges);
#endif

    std::size_t sendWeight{0};
    std::size_t sendSucc{0};
    std::size_t sendPred{0};
    std::size_t recvWeight{0};
    std::size_t recvSucc{0};
    std::size_t recvPred{0};
    // Weight, succ and pred requests.
    std::size_t reqCounter{0};
    static constexpr std::size_t NUM_REQS{3};
    std::array<MPI_Request, NUM_REQS> reqs;
    std::array<MPI_Status, NUM_REQS> statuses;
    for (std::size_t iter = 1; iter <= numSteps; ++iter)
    {
        recvWeight = 0;
        recvSucc = 0;
        recvPred = 0;

        if (myPred != 0)
        {
            // Send my value to my predecesor.
            sendWeight = myWeight;
            MPI_Isend(&sendWeight, 1, MPI_UINT64_T, myPred - 1, TAG_WEIGHT_SUFFIX_SUM, MPI_COMM_WORLD, &reqs[reqCounter++]);
            // Send my successor to my predecesor.
            sendSucc = mySucc;
            MPI_Isend(&sendSucc, 1, MPI_UINT64_T, myPred - 1, TAG_SUCC_SUFFIX_SUM, MPI_COMM_WORLD, &reqs[reqCounter++]);
        }
        if (mySucc != 0)
        {
            // Send my predecessor to my successor.
            sendPred = myPred;
            MPI_Isend(&sendPred, 1, MPI_UINT64_T, mySucc - 1, TAG_PRED_SUFFIX_SUM, MPI_COMM_WORLD, &reqs[reqCounter++]);
        }

        if (mySucc != 0)
        {
            // Receive value from my successor.
            MPI_Recv(&recvWeight, 1, MPI_UINT64_T, mySucc - 1, TAG_WEIGHT_SUFFIX_SUM, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            // Receive successor from my successor.
            MPI_Recv(&recvSucc, 1, MPI_UINT64_T, mySucc - 1, TAG_SUCC_SUFFIX_SUM, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        if (myPred != 0)
        {
            // Receive predecessor from my predecessor.
            MPI_Recv(&recvPred, 1, MPI_UINT64_T, myPred - 1, TAG_PRED_SUFFIX_SUM, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        MPI_Waitall(reqCounter, reqs.data(), statuses.data());
        reqCounter = 0;

        myWeight += recvWeight;
        mySucc = recvSucc;
        myPred = recvPred;

#ifdef DEBUG
        debugPrintStart(myEdge - 1);
        std::cout << "Iter " << iter << " : I am " << myEdge
                  << " succ: " << mySucc
                  << " pred: " << myPred
                  << " weight: " << myWeight << std::endl;
        debugPrintEnd(myEdge - 1, numEdges);
#endif
    }

    return myWeight;
}

/// Transform suffix sum value to pre-order index.
std::size_t transformSuffixSum(std::size_t suffixSum, std::size_t numProcs)
{
    return (numProcs + 2) / 2 - suffixSum + 1;
}

/// Gather and print the result.
void gatherPrintResult(std::size_t edge, std::size_t preOrder, const std::string &input)
{
    MPI_Comm resultComm;
    MPI_Comm_split(MPI_COMM_WORLD, isEdgeForward(edge), preOrder, &resultComm);

    if (isEdgeForward(edge))
    {
        std::vector<char> resultVector(input.begin(), input.end());
        MPI_Gather(&input[edgeChild(edge) - 1], 1, MPI_CHAR, resultVector.data() + 1, 1, MPI_CHAR, 0, resultComm);

        if (edge == 1)
        {
#ifndef BENCH
            std::string resultStr(resultVector.begin(), resultVector.end());
            std::cout << resultStr << std::endl;
#endif
#ifdef DEBUG
            std::string comparison = getComparisonPreorder(input);
            if (resultStr != comparison)
            {
                std::cout << "Result is NOT correct, should be: " << comparison << std::endl;
            }
            else
            {
                std::cout << "Result IS correct!" << std::endl;
            }
#endif
        }
    }

    MPI_Comm_free(&resultComm);
}

/// Main application function.
int mainProgram(std::size_t numProcs, std::size_t myId, const std::string &input)
{
#ifdef DEBUG
    if (myId == 0)
    {
        std::cout << "Debugging enabled!" << std::endl;
        std::cout << "Started with " << numProcs << " processors" << std::endl;
    }
#endif

    std::size_t myEdge{myId + 1};
    std::size_t myEtour{eTour(myEdge, numProcs)};

    if (myEdge == edgeBackToRoot(numProcs))
    {
        myEtour = myEdge;
    }

    std::size_t myWeight{isEdgeForward(myEdge) ? 1u : 0u};

#ifdef DEBUG
    debugPrintStart(myId);
    std::cout << "I am " << myEdge << " eTour: " << myEtour << " weight: " << myWeight << std::endl;
    debugPrintEnd(myId, numProcs);
#endif

    std::size_t suffixSum{suffixSums(myEdge, myEtour, myWeight, numProcs)};

#ifdef DEBUG
    debugPrintStart(myId);
    std::cout << "I am " << myEdge << " suffixSum: " << suffixSum << std::endl;
    debugPrintEnd(myId, numProcs);
#endif

    std::size_t preOrder{transformSuffixSum(suffixSum, numProcs)};

#ifdef DEBUG
    debugPrintStart(myId);
    if (isEdgeForward(myEdge))
    {
        std::cout << "I am node " << edgeChild(myEdge) << " result: " << preOrder << std::endl;
    }
    debugPrintEnd(myId, numProcs);
#endif

    gatherPrintResult(myEdge, preOrder, input);

    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int numProcs{-1};
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    int myId{-1};
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);

    if (argc < PARAM_NUM_REQUIRED || numProcs < 0 || myId < 0)
    {
        if (myId == 0)
        {
            std::cerr << "Usage: mpirun --prefix /usr/local/share/OpenMPI -np NUM_PROCS mss INPUT\n"
                "\tNUM_PROCS: Must be set to (2 * length(INPUT) - 2)" << std::endl;
        }
        return EXIT_FAILURE;
    }

    std::string input{argv[PARAM_INPUT]};

    if ((2 * input.size() - 2) != static_cast<std::size_t>(numProcs))
    {
        std::cerr << "Usage: mpirun --prefix /usr/local/share/OpenMPI -np NUM_PROCS mss INPUT\n"
            "\tNUM_PROCS: Must be set to (2 * length(INPUT) - 2)" << std::endl;

        return EXIT_FAILURE;
    }

#ifdef BENCH
    MPI_Barrier(MPI_COMM_WORLD);
    double start = MPI_Wtime();
#endif

    int retVal{EXIT_FAILURE};
    try {
        retVal = mainProgram(static_cast<std::size_t>(numProcs),
                             static_cast<std::size_t>(myId),
                             input);
    } catch (...) {
        MPI_Abort(MPI_COMM_WORLD, ERROR_SIZE);
    }

#ifdef BENCH
    MPI_Barrier(MPI_COMM_WORLD);
    double end = MPI_Wtime();
#endif

    MPI_Finalize();

#ifdef BENCH
    if (myId == 0)
    {
        std::cout << std::fixed << std::setprecision(8) << end - start << std::endl;
    }
#endif

    return retVal;
}
