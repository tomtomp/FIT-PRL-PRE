#!/bin/bash

if [ $# -lt 1 ]; then 
    echo "Usage: ./test.sh INPUT_STRING";
    exit;
fi;

input_string=$1;
input_length=$(( 2 * ${#1} - 2));

if [ ${input_length} -le 0 ]; then
    echo "Please enter a string with at least 2 characters!";
    exit;
fi;

echo "localhost slots=${input_length}" > ./hostfile

#mpic++ --prefix /usr/local/share/OpenMPI -o pro pro.cpp -DDEBUG
#mpic++ --prefix /usr/local/share/OpenMPI -o pro pro.cpp -DBENCH
mpic++ --prefix /usr/local/share/OpenMPI -o pro pro.cpp
mpirun --prefix /usr/local/share/OpenMPI --hostfile ./hostfile -np ${input_length} ./pro ${input_string}

# Cleanup
rm -f pr

